﻿#pragma once

#include <QObject>
#include <QWidget>

class QsciScintilla;
class XScreenshotClass : public QObject
{
	Q_OBJECT

public:
	XScreenshotClass(QWidget* mainWidget, QsciScintilla* pEdit, QObject* parent);
	~XScreenshotClass();

private slots:
	void on_Screenshot_clicked();

};
